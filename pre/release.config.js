// Les plugins utilisés par Semantic Release pour gérer les versions et les déploiements
const pluginCommitAnalyzer = "@semantic-release/commit-analyzer";
const pluginReleaseNotesGenerator = "@semantic-release/release-notes-generator";
const pluginChangelog = "@semantic-release/changelog";
const pluginSemanticReleaseUpdateFile = [
  "@droidsolutions-oss/semantic-release-update-file",
  {
    "files": []
  }
];
const pluginGit = [
  "@semantic-release/git",
  {
    "assets": [
      "package.json",
      "package-lock.json",
      "CHANGELOG.md",
    ],
    "message": "chore(release): ${nextRelease.version} \n\n${nextRelease.notes}"
  }
];
const pluginGitLab = [
  "@semantic-release/gitlab",
  {
    "assets": []
  }
];

// Les plugins utilisés en environnement de développement
const pluginsDevelop = [pluginCommitAnalyzer, pluginSemanticReleaseUpdateFile, pluginGit];

// Les plugins utilisés en environnement de production
const pluginsMain = [
  pluginCommitAnalyzer,
  pluginReleaseNotesGenerator,
  pluginChangelog,
  pluginSemanticReleaseUpdateFile,
  pluginGit,
  pluginGitLab
];

// Les plugins à utiliser dépendant de la branche Git courante
const { CI_COMMIT_BRANCH } = process.env;
var plugins = {};
if (CI_COMMIT_BRANCH === "develop" || CI_COMMIT_BRANCH.startsWith("features/")){
  plugins = pluginsDevelop
} else {
  plugins = pluginsMain
}
// const plugins = CI_COMMIT_BRANCH === "develop" ? pluginsDevelop : pluginsMain;

module.exports = {
  branches: [ "main", 
              "master",
                {
                 name: "develop",
                 prerelease: "beta" 
                },
                {
                  name: "features/**",
                  prerelease: "alpha"
                }
            ],   
  plugins
};
